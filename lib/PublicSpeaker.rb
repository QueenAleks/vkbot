require_relative 'Network.rb'
require_relative 'SpeachAnalyzer.rb'

class PublicSpeaker
  def self.Speak
    while 1 == 1
      messages = NetworkRequests.CheckMessages
      if messages.nil?
	    sleep 10
        next
      end
      
	  messages.reverse!
      messages.each do |m|
        user = m["user_id"]
        chat = m["chat_id"]
	    text = m["body"]
	    
		f = File.open("#{Dir.pwd}/lib/base/AllMessages.txt", 'a')
		f.puts "#{Time.now.getutc}: #{user}@ #{text}"
		f.close
		
		usr_name = nil
		while usr_name.nil?
		  sleep 1
	      usr_name = NetworkRequests.GetName(user)
	    end
		phrase = SpeachAnalyzer.Analysis(text, usr_name)
	    if phrase.nil?
	      next
	    end
		
		if phrase == 'hello'
		  1.upto(7) do |i|
		    speech = File.new("#{Dir.pwd}/lib/base/Hello#{i}.txt").read
		    while !NetworkRequests.SendMessage(speech, chat.nil? ? user : chat, chat.nil? ? false : true)
	          sleep 1
	        end
			sleep 0.5
		  end
		  
		  next
		end
		
	    speech = "#{usr_name}, #{phrase}!"
	    
	    while !NetworkRequests.SendMessage(speech, chat.nil? ? user : chat, chat.nil? ? false : true)
	      sleep 1
	    end
		
		f = File.open("#{Dir.pwd}/lib/base/log.txt", 'a')
		f.puts "#{Time.now.getutc}: #{usr_name}@ #{text} - #{speech}"
		f.close
		
		sleep 1
      end
	  
	  sleep 10
	end
  end
end
