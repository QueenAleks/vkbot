require_relative 'OrderKeeper.rb'
require 'time'

class SpeachAnalyzer
  THREE_DAYS = 3 * 24 * 60 * 60

  def self.Analysis(phrase, usr_name)
	if (/^Дорогой Бот, запиши \d{1,2} на (ММА|СпецДМ|ИСП|Программирование|БЖЧ)$/ =~ phrase) == 0
	  
	  w = phrase.split(' ')
	  pos = w[3].to_i
	  
	  if pos < 1 || pos > 30
	    return 'ты нормальный?'
	  end
	  
	  ekz = w[5]
	  
	  if !CheckDate(ekz)
	    return 'ещё рано'
	  end
	  
	  answ = OrderKeeper.add(ekz, usr_name, pos)
	  if answ == 'no'
		return 'не наглей'
	  elsif answ
	    return 'записано'
	  else
	    return 'занято'
	  end
	  
	elsif (/^Дорогой Бот, удали из очереди на (ММА|СпецДМ|ИСП|Программирование|БЖЧ)$/ =~ phrase) == 0
	  
	  ekz = phrase.split(' ')[6]
	  OrderKeeper.remove(ekz, usr_name)
	  return 'удалил'
	  
	elsif (/^Дорогой Бот, покажи очередь на (ММА|СпецДМ|ИСП|Программирование|БЖЧ)$/ =~ phrase) == 0
	
	  ekz = phrase.split(' ')[5]
	  answer = "Текущая очередь на #{ekz}: \n"
	  students = OrderKeeper.read(ekz)
	  students.map.with_index do |s, i|
	    if !s.nil?
		  answer += "#{i} - #{s}\n"
		end
	  end
	  
	  return answer
	
	elsif 'Здравствуй, Дорогой Бот!' == phrase || 'Дорогой Бот, что ты умеешь?' == phrase
	  return 'hello'
	end
	
	nil
  end
  
  def self.CheckDate(disciplne)
    dates = JSON.parse(File.new("#{Dir.pwd}/lib/base/Dates.txt").read)
	t_now = Time.now.getutc
	if (Time.parse(dates[disciplne]) - t_now) > THREE_DAYS
	  false
	else
	  true
	end
  end
end