require 'net/http'

class NetworkRequests
  ACCESS_TOKEN = '' # Secret Token. Use VK api to get it for your account.
  CHAT_COEFF = 2000000000
  
  def self.CheckMessages
	id = File.new("#{Dir.pwd}/lib/base/last_message_id.txt").read
    url = URI('https://api.vk.com/method/messages.get')
    params = { 'v' => 5.69, 'access_token' => ACCESS_TOKEN, 'count' => 200, 'last_message_id' => id }
    url.query = URI.encode_www_form(params)
    
	my_response = Net::HTTP.get_response(url).body
	if my_response['response'].nil?
	  f = File.open("#{Dir.pwd}/lib/base/errors.txt", 'a')
	  f.puts "#{Time.now.getutc}: #{url} - #{my_response['error']}"
	  f.close
	  puts "#{Time.now.getutc}: #{url} - #{my_response['error']}"
	  sleep 1
	  return nil
	end
	
    res = JSON.parse(my_response)['response']['items']
	if !res.empty?
	  f = File.new("#{Dir.pwd}/lib/base/last_message_id.txt", 'w')
	  f.print(res[0]["id"])
	  f.close
	end
	
	sleep 1
	res
  end
  
  def self.SendMessage(message, peer_id, is_chat)
    url = URI('https://api.vk.com/method/messages.send')
	if is_chat
	  params = { 'message' => message, 'peer_id' => CHAT_COEFF + peer_id, 'v' => 5.69, 'access_token' => ACCESS_TOKEN }
	else
	  params = { 'message' => message, 'peer_id' => peer_id, 'v' => 5.69, 'access_token' => ACCESS_TOKEN }
    end	
    url.query = URI.encode_www_form(params)
	res = JSON.parse(Net::HTTP.get_response(url).body)
	
	if res["response"].nil?
	  f = File.open("#{Dir.pwd}/lib/base/errors.txt", 'a')
	  f.puts "#{Time.now.getutc}: #{url} - #{res['error']}"
	  f.close
	  puts "#{Time.now.getutc}: #{url} - #{res['error']}"
	  sleep 1
	  return false
	end
	
	sleep 1
	true
  end
  
  def self.GetName(id)
    url = URI('https://api.vk.com/method/users.get')
    params = { 'v' => 5.69, 'access_token' => ACCESS_TOKEN, 'user_ids' => id }
    url.query = URI.encode_www_form(params)
    
	my_response = Net::HTTP.get_response(url).body
	if my_response['response'].nil?
	  f = File.open("#{Dir.pwd}/lib/base/errors.txt", 'a')
	  f.puts "#{Time.now.getutc}: #{url} - #{my_response['error']}"
	  f.close
	  puts "#{Time.now.getutc}: #{url} - #{my_response['error']}"
	  sleep 1
	  return nil
	end
	
	sleep 1
	res = JSON.parse(my_response)['response'][0]
	
	
	"#{res["first_name"]} #{res["last_name"]}"
  end
end