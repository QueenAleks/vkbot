require 'json'

class OrderKeeper

  def self.add (discipline, usr_name, order)
    f = File.new("#{Dir.pwd}/lib/base/#{discipline}.txt")
    queue = JSON.parse(f.read)
	f.close
    if !queue[order].nil?
	  return false
	end
	queue.each do |b|
	  if b == usr_name
	    return 'no'
	  end
	end
	queue[order] = usr_name
	f = File.new("#{Dir.pwd}/lib/base/#{discipline}.txt", 'w')
	f.print(queue.to_json)
	f.close
	true
  end
  
  def self.remove(discipline, usr_name)
    f = File.new("#{Dir.pwd}/lib/base/#{discipline}.txt")
    queue = JSON.parse(f.read)
	f.close
	queue.map.with_index do |k,i|
	  if k == usr_name
	    queue[i] = nil
	  end
	end
	f = File.new("#{Dir.pwd}/lib/base/#{discipline}.txt", 'w')
	f.print queue.to_json
	f.close
	true
  end
  
  def self.read (discipline)
    JSON.parse(File.new("#{Dir.pwd}/lib/base/#{discipline}.txt").read)
  end
end